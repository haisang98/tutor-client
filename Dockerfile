FROM nginx:1.15.2-alpine

COPY ./build /var/www
ADD ./conf.d/default.conf /etc/nginx/nginx.conf
EXPOSE 80
ENTRYPOINT ["nginx","-g","daemon off;"]