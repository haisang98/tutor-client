import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, SvgIcon } from '@material-ui/core';
import AppleImage from 'assets/images/apple.svg';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 20,
        height: 21,
    },
}));

function Apple() {
    const classes = useStyles();

    return <img src={AppleImage} className={classes.root} alt="Apple icon" />;
}

Apple.propTypes = {};

export default Apple;
