import React from 'react';
import PropTypes from 'prop-types';
import eLearningImage from 'assets/images/e-learning.svg';

function ELearning(props) {
    // const classes = useStyles();

    return <img src={eLearningImage} alt="e-learning icon" />;
}

ELearning.propTypes = {};

export default ELearning;
