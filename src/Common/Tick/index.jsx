import React from 'react';
import PropTypes from 'prop-types';
import TickImage from 'assets/images/tick.svg';
import { variables } from 'Material/Style/colors';
import { makeStyles, SvgIcon } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        width: ({ width }) => width,
        height: ({ height }) => height,
    },

    path: {
        fill: ({ variant }) => variant === 'main' && variables.colorMain,
    },
}));

function Tick({ variant = 'main', width = 24, height = 25 }) {
    const classes = useStyles({ variant, width, height });

    // return <img src={TickImage} className={classes.root} alt="Tick Icon" />;
    return (
        <SvgIcon
            className={classes.root}
            viewBox="0 0 24 25"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <g clipPath="url(#clip0)">
                <path
                    className={classes.path}
                    d="M17.9925 8.55491C17.6231 8.16538 17.0072 8.14822 16.6171 8.51858L10.4056 14.4093L7.44293 11.3675C7.0677 10.9825 6.45246 10.974 6.06715 11.3493C5.68216 11.7242 5.67405 12.3401 6.04896 12.7251L9.6814 16.4545C9.87176 16.6501 10.1247 16.7487 10.3784 16.7487C10.619 16.7487 10.8597 16.6598 11.0481 16.4817L17.9562 9.93041C18.3461 9.56066 18.3626 8.94477 17.9925 8.55491Z"
                />
                <path
                    className={classes.path}
                    d="M12 0.5C5.38312 0.5 0 5.88312 0 12.5C0 19.1169 5.38312 24.5 12 24.5C18.6169 24.5 24 19.1169 24 12.5C24 5.88312 18.6169 0.5 12 0.5ZM12 22.5541C6.45633 22.5541 1.94592 18.044 1.94592 12.5C1.94592 6.95633 6.45628 2.44592 12 2.44592C17.544 2.44592 22.0541 6.95628 22.0541 12.5C22.0541 18.044 17.544 22.5541 12 22.5541Z"
                />
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect
                        width="24"
                        height="24"
                        fill="white"
                        transform="translate(0 0.5)"
                    />
                </clipPath>
            </defs>
        </SvgIcon>
    );
}

Tick.propTypes = {};

export default Tick;
