import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 324,
        minWidth: 324,
        marginTop: '.75rem',
        height: '2.5rem',
        borderRadius: '1.5rem',
        border: 'none',
        outline: 'none',
        backgroundColor: '#082032',
        cursor: 'pointer',
        color: '#FFFFFF',
        fontSize: '1rem',
        fontWeight: 700,
    },
}));

function Payfast(props) {
    const classes = useStyles();
    return (
        <form action="https://sandbox.payfast.co.za​/eng/process" method="post">
            <input type="hidden" name="merchant_id" value="10000100" />
            <input type="hidden" name="merchant_key" value="46f0cd694581a" />
            <input type="hidden" name="amount" value="100.00" />
            <input type="hidden" name="item_name" value="Tutor" />
            <input
                type="hidden"
                name="return_url"
                value="https://pockettutor.co.za/"
            />
            <input
                type="hidden"
                name="cancel_url"
                value="https://pockettutor.co.za/"
            />
            <input
                type="hidden"
                name="notify_url"
                value="https://pockettutor.co.za/"
            />
            <input type="hidden" name="name_first" value="Gnas" />
            <input type="hidden" name="name_last" value="Drimaes" />
            <input type="hidden" name="email_address" value="Gnas@gmail.com" />
            <input type="hidden" name="payment_method" value="cc" />
            <input type="submit" className={classes.root} value="Payment Now" />
        </form>
    );
}

Payfast.propTypes = {};

export default Payfast;
