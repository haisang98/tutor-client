import React from 'react';
import PropTypes from 'prop-types';
import questionImage from 'assets/images/question.svg';

function Question(props) {
    // const classes = useStyles();

    return <img src={questionImage} alt="question icon" />;
}

Question.propTypes = {};

export default Question;
