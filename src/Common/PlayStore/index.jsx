import React from 'react';
import PropTypes from 'prop-types';
import PlayStoreImage from 'assets/images/google-play.svg';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 20,
        height: 21,
    },
}));

function PlayStore(props) {
    const classes = useStyles();

    return (
        <img
            src={PlayStoreImage}
            className={classes.root}
            alt="Playstore icon"
        />
    );
}

PlayStore.propTypes = {};

export default PlayStore;
