import React from 'react';
import PropTypes from 'prop-types';
import BrandImage from 'assets/images/Brand.svg';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core';
import { variables } from 'Material/Style/colors';

const useStyles = makeStyles((theme) => ({
    link: {
        color: variables.colorBlack,
        textDecoration: 'none',
    },

    image: {
        [theme.breakpoints.down('xs')]: {
            width: '10rem',
        },
    },
}));
function Brand(props) {
    const classes = useStyles();
    return (
        <Link to="/" className={classes.link}>
            <img src={BrandImage} alt="brand" className={classes.image} />
        </Link>
    );
}

Brand.propTypes = {};

export default Brand;
