import React from 'react';
import PropTypes from 'prop-types';
import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import { variables } from 'Material/Style/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        border: `1px solid ${variables.borderColorDefault}`,
        borderRadius: '20px',
        padding: theme.spacing(3),
        flexBasis: '17%',
        minWidth: 350,
        textAlign: 'center',
        marginRight: theme.spacing(2),
        marginBottom: theme.spacing(2),
        height: '25rem',
        [theme.breakpoints.down('sm')]: {
            flexBasis: '75%',
            marginRight: theme.spacing(0),
        },
    },

    image: {},
    question: {
        color: variables.colorBlack,
        display: 'block',
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(0.5),
    },

    answer: {
        display: 'inline-block',
    },
}));

function Card({ component, question, answer }) {
    const classes = useStyles();
    return (
        <Grid item xs={12} lg={6} xl={3} className={classes.root}>
            <Box className={classes.image}>{component}</Box>
            <Typography component="span" className={classes.question}>
                {question}
            </Typography>
            <Typography component="span" className={classes.answer}>
                {answer}
            </Typography>
        </Grid>
    );
}

Card.propTypes = {};

export default Card;
