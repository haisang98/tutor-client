import React from 'react';
import PropTypes from 'prop-types';
import MortarBoardImage from 'assets/images/mortarboard.svg';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        width: 50,
        height: 51,
    },
}));

function MortarBoard(props) {
    const classes = useStyles();
    return (
        <img
            src={MortarBoardImage}
            className={classes.root}
            alt="MortarBoard Icon"
        />
    );
}

MortarBoard.propTypes = {};

export default MortarBoard;
