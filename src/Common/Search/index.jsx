import React from 'react';
import PropTypes from 'prop-types';
import searchImage from 'assets/images/search.svg';

function Search(props) {
    // const classes = useStyles();

    return <img src={searchImage} alt="search icon" />;
}

Search.propTypes = {};

export default Search;
