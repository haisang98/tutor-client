import React, { useState } from 'react';
import Home from 'pages/Home';
import ForgotPassword from 'pages/ForgotPassword/Test';
import {
    Switch,
    Route,
    BrowserRouter as Router,
    Redirect,
    useLocation,
    useRouteMatch,
    useParams,
    useHistory,
} from 'react-router-dom';
import Login from 'pages/Login';
import Register from 'pages/Register';
import RegisterComplete from 'pages/RegisterComplete/Test';
import {
    createTheme,
    ThemeProvider,
    responsiveFontSizes,
} from '@material-ui/core';
import useAuth from 'hook/useAuth';
import PublicRoute from 'pages/PublicRoute';
import PrivateRoute from 'pages/PrivateRoute';
import { getToken } from 'helpers/token';
import { useEffect } from 'react';
import apis from 'apis';
import Error from 'components/401';
import NotFound from 'components/404';

let theme = createTheme({
    breakpoints: {
        values: {
            xs: 0,
            sm: 600,
            md: 960,
            lg: 1280,
            xl: 1920,
        },
    },
});

theme = responsiveFontSizes(theme);

function App() {
    const [isAuthenticated, setIsAuthenticated] = useState(
        (getToken() && true) || false,
    );
    const [user, setUser] = useState('');
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const handleIsAuthenticated = (value) => {
        setIsAuthenticated(value);
    };

    useEffect(() => {
        // const check = (getToken() && true) || false;
        // setIsAuthenticated(check);
        setLoading(true);
        const fetchProfile = async () => {
            try {
                const data = await apis.getProfile();

                setUser(data.email);

                setLoading(false);
            } catch (error) {
                setError({
                    statusCode: error.statusCode,
                    message: error.message,
                });
                setLoading(false);
            }
        };
        if (getToken()) {
            setIsAuthenticated(true);
            fetchProfile();
        } else {
            setLoading(loading);
        }
        // setUser();
    }, [getToken()]);

    if (error) return <Error />;
    console.log(isAuthenticated);
    return (
        <ThemeProvider theme={theme}>
            <Switch>
                <Route exact path="/">
                    <Home
                        isAuthenticated={isAuthenticated}
                        handleIsAuthenticated={handleIsAuthenticated}
                        user={user}
                        loading={loading}
                    />
                </Route>

                <Route exact path="/forgot-password">
                    {(!isAuthenticated && <ForgotPassword />) || (
                        <Redirect to="/" />
                    )}
                </Route>

                <Route exact path="/login">
                    {(!isAuthenticated && (
                        <Login handleIsAuthenticated={handleIsAuthenticated} />
                    )) || <Redirect to="/" />}
                </Route>

                <Route exact path="/register">
                    {(!isAuthenticated && <Register />) || <Redirect to="/" />}
                </Route>

                <Route exact path="/register-complete">
                    {(isAuthenticated && (
                        <RegisterComplete
                            isAuthenticated={isAuthenticated}
                            handleIsAuthenticated={handleIsAuthenticated}
                            loading={loading}
                            user={user}
                        />
                    )) || (
                        <Redirect
                            to={{
                                pathname: '/',
                                state: { from: '/register-complete' },
                            }}
                        />
                    )}
                </Route>

                <Route component={NotFound} />
            </Switch>
        </ThemeProvider>
    );
}

export default App;
