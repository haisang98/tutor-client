import axios from 'axios';

const TIMEOUT = 1000 * 100; // 15 seconds

const instance = axios.create({
    baseURL: `${process.env.REACT_APP_URL}`,
    timeout: TIMEOUT,
});

instance.interceptors.response.use(
    (response) => {
        return response.data;
    },
    (error) => {
        console.log(error);
        return Promise.reject(error);
    },
);

instance.interceptors.request.use(
    (config) => {
        if (!config.headers.Authorization) {
            const token = localStorage.getItem('REACT_ACCESS_TOKEN');

            if (token) {
                config.headers.Authorization = `Bearer ${token}`;
            }
        }

        return config;
    },
    (error) => Promise.reject(error),
);

export default instance;
