import instance from './config';
import { setToken, removeToken, getToken } from 'helpers/token';

const CHAT = '/chat';
const AUTH = '/auth';
const FORGOT_PASSWORD = '/forgot-password';
const PROFILE = '/profile';
const STUDENT = '/student';
const INSTRUCTOR = '/tutor';
class API {
    #instance;

    constructor() {
        this.#instance = instance;
    }

    login = async ({ email, password }) => {
        return this.#instance.post(`${AUTH}/login`, { email, password });
    };

    loginAsTutor = async ({ email, password }) => {
        return this.#instance.post(`${AUTH}${INSTRUCTOR}/login`, {
            email,
            password,
        });
    };

    loginAsStudent = async ({ email, password }) => {
        return this.#instance.post(`${AUTH}${STUDENT}/login`, {
            email,
            password,
        });
    };

    register = async (values) => {
        return this.#instance.post(`${AUTH}/signup`, values);
    };

    requestCode = async ({ email }) => {
        return this.#instance.post(`${AUTH}${FORGOT_PASSWORD}/request-code`, {
            email,
        });
    };

    verifyCode = async ({ email, code }) => {
        return this.#instance.post(`${AUTH}${FORGOT_PASSWORD}/verify-code`, {
            email,
            code: Number(code),
        });
    };

    resetPassword = async ({ email, password, token }) => {
        return this.#instance.post(`${AUTH}${FORGOT_PASSWORD}/reset-password`, {
            email,
            token,
            password,
        });
    };

    getProfile = async () => {
        return this.#instance.get(`${PROFILE}`);
    };

    createConversation = async () => {
        return this.#instance.post(`${CHAT}/conversation`, {
            friendlyName: 'Test Chat',
        });
    };

    fetchConversation = async (sid) => {
        return this.#instance.post(`${CHAT}/conversation/fetch`, {
            sid,
        });
    };

    requestToken = async (chatServiceSid) => {
        return this.#instance.post(`${CHAT}/request-token`, { chatServiceSid });
    };
}

export default new API();
