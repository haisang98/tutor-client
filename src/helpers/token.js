const setToken = (token) => {
    if (!token) return;

    localStorage.setItem('REACT_ACCESS_TOKEN', token);
};

const removeToken = () => {
    const token = localStorage.getItem('REACT_ACCESS_TOKEN');

    if (!token) return;

    localStorage.removeItem('REACT_ACCESS_TOKEN');
};

const getToken = () => {
    const token = localStorage.getItem('REACT_ACCESS_TOKEN');

    if (!token) return null;

    return token;
};

export { setToken, removeToken, getToken };
