import { withStyles, Button } from '@material-ui/core';
import { variables } from 'Material/Style/colors';

const Based = withStyles((theme) => ({
    root: {
        color: variables.colorWhite,
        backgroundColor: variables.bgMain,
        borderRadius: variables.brRadiusStandard,
        textTransform: 'capitalize',
        '&:hover': {
            color: variables.bgMain,
            backgroundColor: variables.bgWhite,
        },
    },
}))(Button);

export default Based;
