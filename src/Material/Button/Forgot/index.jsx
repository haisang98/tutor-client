import { blue } from '@material-ui/core/colors';
import { withStyles, Button } from '@material-ui/core';

const ButtonForgot = withStyles((theme) => ({
    root: {
        color: theme.palette.getContrastText(blue[700]),
        backgroundColor: blue[700],
        '&:hover': {
            backgroundColor: blue[800],
        },
    },
}))(Button);

export default ButtonForgot;
