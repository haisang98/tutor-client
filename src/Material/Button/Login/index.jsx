import { withStyles, Button } from '@material-ui/core';
import { variables } from 'Material/Style/colors';

const SignIn = withStyles((theme) => ({
    root: {
        color: variables.colorWhite,
        backgroundColor: variables.bgMain,
        border: `1px solid ${variables.bgMain}`,
        borderRadius: variables.brRadiusStandard,
        textTransform: 'capitalize',
        '&:hover': {
            border: `1px solid #FFFFFF`,
        },
    },
}))(Button);

export default SignIn;
