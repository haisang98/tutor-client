import { withStyles, Button } from '@material-ui/core';
import { variables } from 'Material/Style/colors';

const Register = withStyles((theme) => ({
    root: {
        color: variables.colorMain,
        border: `1px solid #FFFFFF`,
        backgroundColor: variables.colorWhite,
        borderRadius: variables.brRadiusStandard,
        textTransform: 'capitalize',
        '&:hover': {
            color: variables.colorWhite,
            border: `1px solid #FFFFFF`,
            backgroundColor: variables.bgMain,
        },
    },
}))(Button);

export default Register;
