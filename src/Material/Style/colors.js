const variables = {
    bgMain: '#1E8CEB',
    bgMainDark: '#1b7ed3',
    bgWhite: '#FFFFFF',
    borderColorDefault: '#D2D2D2',
    bgCircle: 'rgba(17, 141, 152, 0.15)',
    bgImage: 'rgba(17, 141, 152, 0.4)',
    colorBlack: '#222222',
    colorWhite: '#FFFFFF',
    colorMain: '#1E8CEB',
    brRadiusStandard: '25px',
};

export { variables };
