import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
    makeStyles,
    Typography,
    useTheme,
    Box,
    Grid,
    Hidden,
} from '@material-ui/core';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import InputField from 'components/InputField';
import Button from 'Material/Button/Base';
import { Link } from 'react-router-dom';
import { variables } from 'Material/Style/colors';
import useForm from 'hook/useForm';
import validateLogin from 'validation/login';
import API from 'apis';
import Register from 'Material/Button/Register';
import backgroundSmartphone from 'assets/images/backgroundSmartphone.svg';
import backgroundSmartphoneMobile from 'assets/images/smartphone-mobile.svg';
import Brand from 'Common/Brand';
import SelectField from 'components/SelectField';
import ButtonGroup from 'components/ButtonGroup';
import SnackBar from 'components/Snackbar';
import { useHistory } from 'react-router-dom';
import Loading from '@material-ui/core/CircularProgress';
import { useNotification } from 'hook/useNotification';

const useStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: '5%',
    },

    container: {
        height: '100%',
        width: '100%',
        boxSizing: 'border-box',
    },

    img: {
        width: '100%',
        '@media (max-width: 905px)': {
            width: '90%',
        },
    },

    img_mobile: {
        display: 'block',
        margin: '0 auto',
        width: '80%',
    },

    buttonGroup: {
        marginTop: theme.spacing(2),
    },

    container__inputField: {
        marginTop: theme.spacing(3),
        width: theme.spacing(40),
    },

    actionButton: {
        marginTop: theme.spacing(2.5),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },

    wrapper__link: {
        marginTop: theme.spacing(2.5),
    },

    link: {
        color: variables.bgMain,
        textDecoration: 'none',
    },
}));

const initialState = {
    role: '',
    email: '',
    password: '',
    firstName: '',
    lastName: '',
};
function SignUp(props) {
    const classes = useStyles();
    const history = useHistory();
    const [hasError, setHasError] = useState(false);
    const [isActive, setIsActive] = useState(false);
    const [loading, setLoading] = useState(false);
    const [snackbar, setSnackbar] = useState({
        open: false,
        message: '',
        statusCode: null,
    });

    useEffect(() => {
        if (snackbar.statusCode === '' && hasError === false) {
            history.replace('/login');
        }
    }, [snackbar.statusCode]);

    const handleOnClose = () => {
        setSnackbar({
            open: false,
            message: '',
            statusCode: '',
        });

        setIsActive(false);
    };

    const handleSignup = async () => {
        setIsActive(true);
        setLoading(true);
        try {
            const result = await API.register(values);

            setHasError(false);

            setSnackbar({
                open: true,
                message: 'Signup success',
                statusCode: 201,
            });

            setLoading(false);
        } catch (error) {
            setLoading(false);
            setHasError(true);
            // console.log('hai sang la tao');
            // console.log(
            //     { error },
            //     error?.response?.data?.message,
            //     error?.response?.statusText,
            //     error?.response?.data?.statusCode,
            //     error?.response?.status,
            // );
            setSnackbar({
                open: true,
                message:
                    error?.response?.data?.message ||
                    'This process has error occur',
                statusCode: error?.response?.status || 500,
            });
            // setSnackbar({
            //     open: true,
            //     message: 'loi',
            //     statusCode: 500,
            // });
        }
    };

    const { errors, handleChange, handleSubmit, values } = useForm(
        handleSignup,
        validateLogin,
        initialState,
    );

    return (
        <>
            <Grid container className={classes.container}>
                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={5}
                    lg={5}
                    xl={5}
                    className={classes.root}
                >
                    <Box mt={3}>
                        <Brand />
                        <ButtonGroup />
                    </Box>

                    {/* ------- */}
                    <div style={{ marginTop: '2em' }}>
                        <Typography
                            component="h4"
                            variant="h4"
                            style={{ fontWeight: 600 }}
                        >
                            Register
                        </Typography>
                        <form onSubmit={handleSubmit}>
                            <div className={classes.container__inputField}>
                                <SelectField
                                    htmlFor="id-htmlFor-role"
                                    label="Register as"
                                    name="role"
                                    value={values.role}
                                    onChange={handleChange}
                                />
                                <Box mb={3}>
                                    <InputField
                                        htmlFor="id-htmlFor-email"
                                        idHelperText="id-helperText-email"
                                        nameHelperText="This is email"
                                        label="Email"
                                        name="email"
                                        value={values.email}
                                        onChange={handleChange}
                                    />
                                </Box>
                                <Box display="flex" mb={3}>
                                    <Box flexBasis="48.5%" marginRight={1}>
                                        <InputField
                                            htmlFor="id-htmlFor-firstName"
                                            idHelperText="id-helperText-firstName"
                                            nameHelperText="This is First Name"
                                            label="First name"
                                            name="firstName"
                                            value={values.firstName}
                                            onChange={handleChange}
                                        />
                                    </Box>
                                    <Box flexBasis="51.5%">
                                        <InputField
                                            htmlFor="id-htmlFor-lastName"
                                            idHelperText="id-helperText-lastName"
                                            nameHelperText="This is Last Name"
                                            label="Last name"
                                            name="lastName"
                                            value={values.lastName}
                                            onChange={handleChange}
                                        />
                                    </Box>
                                </Box>

                                <Box mb={3}>
                                    <InputField
                                        htmlFor="id-htmlFor-password"
                                        idHelperText="id-helperText-password"
                                        nameHelperText="This is password"
                                        label="Password"
                                        name="password"
                                        type="password"
                                        value={values.password}
                                        onChange={handleChange}
                                    />
                                </Box>
                                <Button
                                    type="submit"
                                    disabled={loading}
                                    fullWidth
                                    className={classes.actionButton}
                                    startIcon={
                                        (loading && (
                                            <Loading
                                                size={18}
                                                color={variables.bgWhite}
                                            />
                                        )) ||
                                        null
                                    }
                                >
                                    Sign Up
                                </Button>
                                <Typography
                                    component="p"
                                    variant="body2"
                                    className={classes.wrapper__link}
                                >
                                    Have account?&nbsp;
                                    <Link to="/login" className={classes.link}>
                                        Sign In
                                    </Link>
                                </Typography>
                            </div>
                        </form>
                        <Box className={classes.buttonGroup}>
                            <IOSButton margin={1} />
                            <AndroidButton />
                        </Box>
                    </div>
                </Grid>

                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={7}
                    lg={7}
                    xl={7}
                    style={{ textAlign: 'right', position: 'relative' }}
                >
                    <Hidden mdUp>
                        <Box>
                            <img
                                src={backgroundSmartphoneMobile}
                                className={classes.img_mobile}
                                alt="background resolution mobile"
                            />
                        </Box>
                    </Hidden>
                    <Hidden smDown>
                        <img
                            src={backgroundSmartphone}
                            className={classes.img}
                            alt="hai sang"
                        />
                    </Hidden>
                </Grid>
            </Grid>
            <SnackBar
                snackbar={snackbar}
                isActive={isActive}
                handleOnClose={handleOnClose}
            />
        </>
    );
}

SignUp.propTypes = {};

export default SignUp;
