import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRoute = ({ children, isAuthenticated, ...rest }) => {
    return (
        <Route
            {...rest}
            render={() => {
                return isAuthenticated === true ? (
                    children
                ) : (
                    <Redirect to="/" />
                );
            }}
        />
    );
};

export default PrivateRoute;
