import React from 'react';
import PropTypes from 'prop-types';
import Header from 'components/Header';
import { makeStyles, Typography } from '@material-ui/core';
import clipPathImage from 'assets/images/clippath.svg';
import smartPhoneImage from 'assets/images/smartphone.svg';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import InputField from 'components/InputField';
import Button from 'Material/Button/Base';
import { variables } from 'Material/Style/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundImage: `url(${smartPhoneImage}), url(${clipPathImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '80%, right',
        minHeight: 900,
        padding: theme.spacing(4, 8, 4, 26),
    },

    groupButton: {
        textAlign: 'end',
        position: 'relative',
        top: '-9rem',
        right: '24.3rem',
    },

    container__content: {
        marginTop: theme.spacing(3),
        width: theme.spacing(40),
    },

    title: {
        fontWeight: 600,
        marginBottom: theme.spacing(2.5),
    },

    title__intro: {
        color: variables.colorBlack,
        marginBottom: theme.spacing(2.5),
        fontSize: '0.97rem',
    },

    actionButton: {
        marginTop: theme.spacing(1),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },
}));

function RegisterComplete(props) {
    const classes = useStyles();

    return (
        <>
            <div className={classes.root}>
                <Header />
                <div className={classes.container__content}>
                    <Typography
                        component="h4"
                        variant="h4"
                        className={classes.title}
                    >
                        Register Complete
                    </Typography>
                    <Typography
                        component="p"
                        variant="p"
                        className={classes.title__intro}
                    >
                        Your account was registerd and an email with the
                        verification instructions was sent to
                        hellovietnam@an.edu.vn. Make sure to check your spam
                        folder if the email does not show up in your inbox.
                    </Typography>
                    <Button className={classes.actionButton} fullWidth>
                        Verify
                    </Button>
                </div>
            </div>
            <div className={classes.groupButton}>
                <IOSButton />
                <AndroidButton />
            </div>
        </>
    );
}

RegisterComplete.propTypes = {};

export default RegisterComplete;
