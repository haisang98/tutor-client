import React from 'react';
import PropTypes from 'prop-types';
import Header from 'components/Header';
import {
    makeStyles,
    Typography,
    useTheme,
    useMediaQuery,
    Box,
    Grid,
    Hidden,
} from '@material-ui/core';
import clipPathImage from 'assets/images/clippath.svg';
import smartPhoneImage from 'assets/images/smartphone.svg';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import InputField from 'components/InputField';
import Button from 'Material/Button/Base';
import { Link } from 'react-router-dom';
import { variables } from 'Material/Style/colors';
import useForm from 'hook/useForm';
import validateLogin from 'validation/login';
import API from 'apis';
import clsx from 'clsx';
import Register from 'Material/Button/Register';
import backgroundSmartphone from 'assets/images/backgroundSmartphone.svg';
import backgroundSmartphoneMobile from 'assets/images/smartphone-mobile.svg';
import SignIn from 'Material/Button/Login';
import Brand from 'Common/Brand';
import ButtonGroup from 'components/ButtonGroup';
import SelectField from 'components/SelectField';

const useStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: '5%',
    },

    container: {
        height: '100%',
        width: '100%',
        boxSizing: 'border-box',
    },

    img: {
        width: '100%',
        '@media (max-width: 905px)': {
            width: '90%',
        },
    },

    img_mobile: {
        display: 'block',
        margin: '0 auto',
        width: '80%',
    },

    buttonGroup: {
        marginTop: theme.spacing(2),
    },

    container__inputField: {
        marginTop: theme.spacing(3),
        width: theme.spacing(40),
    },

    actionButton: {
        marginTop: theme.spacing(2.5),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },

    wrapper__link: {
        marginTop: theme.spacing(2.5),
    },

    link: {
        color: variables.bgMain,
        textDecoration: 'none',
    },

    container__content: {
        marginTop: theme.spacing(3),
        width: theme.spacing(40),
    },

    title: {
        fontWeight: 600,
        marginBottom: theme.spacing(2.5),
    },

    title__intro: {
        color: variables.colorBlack,
        marginBottom: theme.spacing(2.5),
        fontSize: '0.97rem',
    },
}));

const initialState = {
    email: '',
    password: '',
};
function RegisterComplete({
    isAuthenticated,
    handleIsAuthenticated,
    loading,
    user,
}) {
    const classes = useStyles();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('sm'));
    const matchesRenderButton = useMediaQuery('(max-width: 845px)');

    const handleLogin = async () => {
        try {
            const result = await API.login(values);
        } catch (error) {
            console.log(error);
        }
    };

    const { errors, handleChange, handleSubmit, values } = useForm(
        handleLogin,
        validateLogin,
        initialState,
    );

    return (
        <>
            <Grid container className={classes.container}>
                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={5}
                    lg={5}
                    xl={5}
                    className={classes.root}
                >
                    <Box mt={3}>
                        <Brand />
                        <ButtonGroup
                            isAuthenticated={isAuthenticated}
                            handleIsAuthenticated={handleIsAuthenticated}
                            loading={loading}
                            user={user}
                        />
                    </Box>

                    {/* ------- */}
                    <>
                        <div>
                            <div className={classes.container__content}>
                                <Typography
                                    component="h4"
                                    variant="h4"
                                    className={classes.title}
                                >
                                    Register Complete
                                </Typography>
                                <Typography
                                    component="p"
                                    variant="p"
                                    className={classes.title__intro}
                                >
                                    Your account was registerd and an email with
                                    the verification instructions was sent to
                                    hellovietnam@an.edu.vn. Make sure to check
                                    your spam folder if the email does not show
                                    up in your inbox.
                                </Typography>
                                <Button
                                    className={classes.actionButton}
                                    fullWidth
                                >
                                    Verify
                                </Button>
                            </div>
                            <div className={classes.buttonGroup}>
                                <IOSButton margin={1} />
                                <AndroidButton />
                            </div>
                        </div>
                    </>
                    {/* --------- */}
                </Grid>

                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={7}
                    lg={7}
                    xl={7}
                    style={{ textAlign: 'right', position: 'relative' }}
                >
                    <Hidden mdUp>
                        <Box>
                            <img
                                src={backgroundSmartphoneMobile}
                                className={classes.img_mobile}
                                alt="background resolution mobile"
                            />
                        </Box>
                    </Hidden>
                    <Hidden smDown>
                        <img
                            src={backgroundSmartphone}
                            className={classes.img}
                            alt="hai sang"
                        />
                    </Hidden>
                </Grid>
            </Grid>
        </>
    );
}

RegisterComplete.propTypes = {};

export default RegisterComplete;
