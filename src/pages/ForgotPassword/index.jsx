import React from 'react';
import PropTypes from 'prop-types';
import Header from 'components/Header';
import { makeStyles, Typography } from '@material-ui/core';
import clipPathImage from 'assets/images/clippath.svg';
import smartPhoneImage from 'assets/images/smartphone.svg';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import InputField from 'components/InputField';
import Button from 'Material/Button/Base';
import { variables } from 'Material/Style/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundImage: `url(${smartPhoneImage}), url(${clipPathImage})`,
        backgroundRepeat: 'no-repeat',
        backgroundPosition: '80%, right',
        minHeight: 900,
        padding: theme.spacing(4, 8, 4, 26),
    },

    groupButton: {
        textAlign: 'end',
        position: 'relative',
        top: '-9rem',
        right: '24.3rem',
    },

    container__inputField: {
        marginTop: theme.spacing(3),
        width: theme.spacing(40),
    },

    actionButton: {
        marginTop: theme.spacing(2.5),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },
}));

function ForgotPassword(props) {
    const classes = useStyles();

    return (
        <>
            <div className={classes.root}>
                <Header />
                <Typography
                    component="h4"
                    variant="h4"
                    style={{ fontWeight: 600 }}
                >
                    forgot password
                </Typography>
                <div className={classes.container__inputField}>
                    <InputField
                        htmlFor="id-htmlFor-email"
                        idHelperText="id-helperText-email"
                        nameHelperText="This is email"
                        label="Email"
                        name="email"
                        onChange={() => {
                            console.log('Hai Sang');
                        }}
                    />
                    <Button className={classes.actionButton} fullWidth>
                        Send
                    </Button>
                </div>
            </div>
            <div className={classes.groupButton}>
                <IOSButton />
                <AndroidButton />
            </div>
        </>
    );
}

ForgotPassword.propTypes = {};

export default ForgotPassword;
