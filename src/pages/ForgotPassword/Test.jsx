import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Header from 'components/Header';
import {
    makeStyles,
    Typography,
    useTheme,
    useMediaQuery,
    Box,
    Grid,
    Hidden,
} from '@material-ui/core';
import clipPathImage from 'assets/images/clippath.svg';
import smartPhoneImage from 'assets/images/smartphone.svg';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import InputField from 'components/InputField';
import Button from 'Material/Button/Base';
import { Link, useHistory } from 'react-router-dom';
import { variables } from 'Material/Style/colors';
import useForm from 'hook/useForm';
import validateLogin from 'validation/login';
import API from 'apis';
import clsx from 'clsx';
import Register from 'Material/Button/Register';
import backgroundSmartphone from 'assets/images/backgroundSmartphone.svg';
import backgroundSmartphoneMobile from 'assets/images/smartphone-mobile.svg';
import SignIn from 'Material/Button/Login';
import Brand from 'Common/Brand';
import ButtonGroup from 'components/ButtonGroup';
import SelectField from 'components/SelectField';
import Snackbar from 'components/Snackbar';
import { setToken } from 'helpers/token';
import Loading from '@material-ui/core/CircularProgress';

const useStyles = makeStyles((theme) => ({
    container__inputField: {
        marginTop: theme.spacing(3),
        width: theme.spacing(40),
    },

    actionButton: {
        marginTop: theme.spacing(2.5),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },

    root: {
        paddingLeft: '5%',
    },

    container: {
        height: '100%',
        width: '100%',
        boxSizing: 'border-box',
    },

    img: {
        width: '100%',
        '@media (max-width: 905px)': {
            width: '90%',
        },
    },

    img_mobile: {
        display: 'block',
        margin: '0 auto',
        width: '80%',
    },

    buttonGroup: {
        marginTop: theme.spacing(2),
    },

    wrapper__link: {
        marginTop: theme.spacing(2.5),
    },

    link: {
        color: variables.bgMain,
        textDecoration: 'none',
    },
}));

function ForgotPassword(props) {
    const classes = useStyles();
    const history = useHistory();
    const [isActive, setIsActive] = useState(false);
    const [loading, setLoading] = useState(false);
    const [isComplete, setIsComplete] = useState(false);
    const [step, setStep] = useState(1);
    const [state, setState] = useState({
        email: '',
        code: '',
        password: '',
        token: '',
    });
    const [snackbar, setSnackbar] = useState({
        open: false,
        message: '',
        statusCode: null,
    });

    useEffect(() => {
        if (snackbar.statusCode === '' && isComplete === true) {
            history.push('/login');
        }
    }, [snackbar.statusCode]);

    const handleChange = (e) => {
        const { name, value } = e.target;

        setState((prev) => ({
            ...prev,
            [name]: value,
        }));
    };

    const handleOnClose = () => {
        setSnackbar({
            open: false,
            message: '',
            statusCode: '',
        });

        setIsActive(false);
    };

    const handleOnSend = async () => {
        setIsActive(true);
        setLoading(true);
        try {
            if (!state.email) return;

            const information = await API.requestCode({ email: state.email });

            setSnackbar({
                open: true,
                message: information.message,
                statusCode: 200,
            });

            setStep(2);

            setLoading(false);
        } catch (error) {
            setLoading(false);
            setSnackbar({
                open: true,
                message: error.response.data.message || 'Internal Server Error',
                statusCode: error.response.data.statusCode || 500,
            });
        }
    };

    const handleOnVerify = async () => {
        setIsActive(true);
        setLoading(true);
        try {
            if (!state.code && !state.email) return;

            const verify = await API.verifyCode({
                code: state.code,
                email: state.email,
            });

            setState((prev) => ({
                ...prev,
                token: verify.token,
            }));

            setSnackbar({
                open: true,
                statusCode: verify.status,
                message: 'Verify success',
            });

            setStep(3);

            setLoading(false);
        } catch (error) {
            setLoading(false);
            setSnackbar({
                open: true,
                statusCode: error.response.data.statusCode || 500,
                message: error.response.data.message || 'Internal Server Error',
            });
        }
    };

    const handleOnSubmit = async () => {
        setIsActive(true);
        setLoading(true);
        try {
            const { email, password, token } = state;
            console.log(email, password, token);
            const finalResult = await API.resetPassword({
                email,
                password,
                token,
            });

            setState({
                email: '',
                password: '',
                code: '',
                token: '',
            });

            setStep(1);

            setSnackbar({
                open: true,
                statusCode: finalResult.status,
                message: finalResult.message,
            });

            setLoading(false);
            setIsComplete(true);
        } catch (error) {
            setLoading(false);
            const message =
                (Array.isArray(error.response.data.message) &&
                    error.response.data.message[0]) ||
                error.response.data.message;
            setSnackbar({
                open: true,
                statusCode: error.response.data.statusCode || 500,
                message: message || 'Internal Server Error',
            });
        }
    };

    return (
        <>
            <Grid container className={classes.container}>
                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={5}
                    lg={5}
                    xl={5}
                    className={classes.root}
                >
                    <Box mt={3}>
                        <Brand />
                        <ButtonGroup />
                    </Box>

                    <div style={{ marginTop: '2em' }}>
                        <Typography
                            component="h4"
                            variant="h4"
                            style={{ fontWeight: 600 }}
                        >
                            Forgot Password
                        </Typography>
                        <div className={classes.container__inputField}>
                            <InputField
                                readonly={(step > 1 && true) || false}
                                htmlFor="id-htmlFor-email"
                                idHelperText="id-helperText-email"
                                nameHelperText="This is email"
                                label="Email"
                                name="email"
                                value={state.email}
                                onChange={handleChange}
                            />
                            <div style={{ marginTop: 15 }} />
                            {step === 2 && (
                                <InputField
                                    htmlFor="id-htmlFor-code"
                                    idHelperText="id-helperText-code"
                                    nameHelperText="This is code to verify"
                                    label="Code"
                                    name="code"
                                    value={state.code}
                                    onChange={handleChange}
                                />
                            )}
                            {step === 3 && (
                                <InputField
                                    htmlFor="id-htmlFor-password"
                                    idHelperText="id-helperText-password"
                                    nameHelperText="This is New Password"
                                    label="Password"
                                    name="password"
                                    type="password"
                                    value={state.password}
                                    onChange={handleChange}
                                />
                            )}
                            <Button
                                className={classes.actionButton}
                                fullWidth
                                disabled={loading}
                                startIcon={
                                    (loading && (
                                        <Loading
                                            size={18}
                                            color={variables.bgWhite}
                                        />
                                    )) ||
                                    null
                                }
                                onClick={
                                    (step === 1 && handleOnSend) ||
                                    (step === 2 && handleOnVerify) ||
                                    handleOnSubmit
                                }
                            >
                                Send
                            </Button>
                        </div>
                        <div className={classes.buttonGroup}>
                            <IOSButton margin={1} />
                            <AndroidButton />
                        </div>
                    </div>
                </Grid>

                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={7}
                    lg={7}
                    xl={7}
                    style={{ textAlign: 'right', position: 'relative' }}
                >
                    <Hidden mdUp>
                        <Box>
                            <img
                                src={backgroundSmartphoneMobile}
                                className={classes.img_mobile}
                                alt="background resolution mobile"
                            />
                        </Box>
                    </Hidden>
                    <Hidden smDown>
                        <img
                            src={backgroundSmartphone}
                            className={classes.img}
                            alt="hai sang"
                        />
                    </Hidden>
                </Grid>
            </Grid>
            <Snackbar
                snackbar={snackbar}
                isActive={isActive}
                handleOnClose={handleOnClose}
            />
        </>
    );
}

ForgotPassword.propTypes = {};

export default ForgotPassword;
