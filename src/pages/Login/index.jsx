import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
    makeStyles,
    Typography,
    useTheme,
    Box,
    Grid,
    Hidden,
} from '@material-ui/core';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import InputField from 'components/InputField';
import Button from 'Material/Button/Base';
import { Link } from 'react-router-dom';
import { variables } from 'Material/Style/colors';
import useForm from 'hook/useForm';
import validateLogin from 'validation/login';
import API from 'apis';
import backgroundSmartphone from 'assets/images/backgroundSmartphone.svg';
import backgroundSmartphoneMobile from 'assets/images/smartphone-mobile.svg';
import Brand from 'Common/Brand';
import ButtonGroup from 'components/ButtonGroup';
import { setToken } from 'helpers/token';
import { useHistory } from 'react-router-dom';
import SnackBar from 'components/Snackbar';
import Loading from '@material-ui/core/CircularProgress';
import RadioGroupRole from 'components/Radio';

const useStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: '5%',
    },

    container: {
        height: '100%',
        width: '100%',
        boxSizing: 'border-box',
    },

    img: {
        width: '100%',
        '@media (max-width: 905px)': {
            width: '90%',
        },
    },

    img_mobile: {
        display: 'block',
        margin: '0 auto',
        width: '80%',
    },

    buttonGroup: {
        marginTop: theme.spacing(2),
    },

    container__inputField: {
        marginTop: theme.spacing(3),
        width: theme.spacing(40),
    },

    actionButton: {
        marginTop: theme.spacing(2.5),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },

    wrapper__link: {
        marginTop: theme.spacing(2.5),
    },

    link: {
        color: variables.bgMain,
        textDecoration: 'none',
    },
}));

const initialState = {
    email: '',
    password: '',
};

function Login({ handleIsAuthenticated }) {
    const classes = useStyles();
    const theme = useTheme();
    const history = useHistory();
    const [radioRole, setRadioRole] = useState('tutor');
    const [isActive, setIsActive] = useState(false);
    const [loading, setLoading] = useState(false);
    const [hasError, setHasError] = useState(false);
    const [snackbar, setSnackbar] = useState({
        open: false,
        message: '',
        statusCode: null,
    });

    useEffect(() => {
        if (snackbar.statusCode === '' && hasError === false) {
            handleIsAuthenticated(true);
            history.push('/');
        }
    }, [snackbar.statusCode]);

    const handleOnClose = () => {
        setSnackbar({
            open: false,
            message: '',
            statusCode: '',
        });
        setIsActive(false);
    };

    const handleLogin = async () => {
        window.scrollTo({ top: 0, behavior: 'smooth' });
        setIsActive(true);
        setLoading(true);
        try {
            let result;

            if (radioRole === 'tutor') {
                result = await API.loginAsTutor(values);
            } else {
                result = await API.loginAsStudent(values);
            }

            // eslint-disable-next-line camelcase
            const { access_token } = result;

            setToken(access_token);

            setHasError(false);

            setSnackbar({
                open: true,
                message: 'Login success',
                statusCode: 200,
            });

            setLoading(false);
        } catch (error) {
            console.log({ error });
            setLoading(false);
            setHasError(true);
            setSnackbar({
                open: true,
                message: 'Wrong email or password',
                statusCode: error?.error?.response?.data?.statusCode,
            });
        }
    };

    const hanleOnChangeRadioRole = (e) => {
        setRadioRole(e.target.value);
    };

    const { errors, handleChange, handleSubmit, values } = useForm(
        handleLogin,
        validateLogin,
        initialState,
    );

    return (
        <>
            <Grid container className={classes.container}>
                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={5}
                    lg={5}
                    xl={5}
                    className={classes.root}
                >
                    <Box mt={3}>
                        <Brand />
                        <ButtonGroup />
                    </Box>

                    <div style={{ marginTop: '2em' }}>
                        <Typography
                            component="h4"
                            variant="h4"
                            style={{ fontWeight: 600 }}
                        >
                            Login
                        </Typography>
                        <div className={classes.container__inputField}>
                            <RadioGroupRole
                                value={radioRole}
                                onChange={hanleOnChangeRadioRole}
                            />
                            <form onSubmit={handleSubmit}>
                                <InputField
                                    mb={3.5}
                                    htmlFor="id-htmlFor-email"
                                    idHelperText="id-helperText-email"
                                    nameHelperText="This is email"
                                    label="Email"
                                    value={values.email}
                                    name="email"
                                    onChange={handleChange}
                                />
                                <InputField
                                    htmlFor="id-htmlFor-password"
                                    idHelperText="id-helperText-password"
                                    nameHelperText="This is password"
                                    label="Password"
                                    type="password"
                                    value={values.password}
                                    name="password"
                                    onChange={handleChange}
                                />
                                <Button
                                    type="submit"
                                    disabled={loading}
                                    className={classes.actionButton}
                                    fullWidth
                                    startIcon={
                                        (loading && (
                                            <Loading
                                                size={18}
                                                color={variables.bgWhite}
                                            />
                                        )) ||
                                        null
                                    }
                                >
                                    Sign In
                                </Button>
                            </form>
                            <Typography
                                component="p"
                                variant="p"
                                className={classes.wrapper__link}
                            >
                                <Link
                                    to="/forgot-password"
                                    className={classes.link}
                                >
                                    Forgot password?
                                </Link>
                            </Typography>
                            <Typography
                                component="p"
                                variant="p"
                                className={classes.wrapper__link}
                            >
                                Don&#39;t have account?&nbsp;
                                <Link to="/register" className={classes.link}>
                                    Sign Up
                                </Link>
                            </Typography>
                        </div>
                        <Box className={classes.buttonGroup}>
                            <IOSButton margin={1} />
                            <AndroidButton />
                        </Box>
                    </div>
                </Grid>

                <Grid
                    item
                    xs={12}
                    sm={12}
                    md={7}
                    lg={7}
                    xl={7}
                    style={{ textAlign: 'right', position: 'relative' }}
                >
                    <Hidden mdUp>
                        <Box>
                            <img
                                src={backgroundSmartphoneMobile}
                                className={classes.img_mobile}
                                alt="background resolution mobile"
                            />
                        </Box>
                    </Hidden>
                    <Hidden smDown>
                        <img
                            src={backgroundSmartphone}
                            className={classes.img}
                            alt="hai sang"
                        />
                    </Hidden>
                </Grid>
            </Grid>
            <SnackBar
                snackbar={snackbar}
                isActive={isActive}
                handleOnClose={handleOnClose}
            />
        </>
    );
}

Login.propTypes = {};

export default Login;
