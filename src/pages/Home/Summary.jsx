import React from 'react';
import PropTypes from 'prop-types';
import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import { variables } from 'Material/Style/colors';
import Tick from 'Common/Tick';
import homeImage from 'assets/images/home-image.svg';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import clsx from 'clsx';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        boxSizing: 'border-box',
        backgroundImage: `url(${homeImage})`,
        backgroundSize: 'cover',
        minHeight: '1270px',
        backgroundRepeat: 'no-repeat',
        backgroundBlendMode: 'darken',
    },
    title: {
        fontWeight: 600,
        color: variables.colorWhite,
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(5),
    },

    box__student: {
        maxWidth: '30em',
        minWidth: '20em',
        flexBasis: '35%',
        borderRadius: '20px',
        boxSizing: 'border-box',
        backgroundColor: variables.bgWhite,
        padding: theme.spacing(4),
        maxHeight: theme.spacing(55),
        height: theme.spacing(55),
        color: variables.colorBlack,
        marginRight: theme.spacing(3),
        marginBottom: theme.spacing(2),
        '@media (max-width: 664px)': {
            marginRight: theme.spacing(0),
        },
    },

    box__typography: {
        textTransform: 'uppercase',
        fontWeight: 600,
    },

    box__tutor: {
        flexBasis: '35%',
        marginBottom: theme.spacing(2),
        maxWidth: '30em',
        minWidth: '20em',
        borderRadius: '20px',
        boxSizing: 'border-box',
        height: theme.spacing(55),
        maxHeight: theme.spacing(55),
        backgroundColor: variables.bgMain,
        padding: theme.spacing(4),
        color: variables.colorWhite,
    },

    box__common__add_border: {
        '&:hover': {
            transition: 'all 500ms',
            boxShadow: '0px 0px 0px 8px rgba(255, 255, 255, 0.5)',
        },
    },

    link: {
        textDecoration: 'none',
    },
}));

function Summary(props) {
    const classes = useStyles();

    const renderItem = ({ text = '', variant = 'main' }) => {
        return (
            <Box display="flex" alignItems="center" mb={3}>
                <Tick variant={variant} width={20} height={21} />
                <Typography component="span" style={{ paddingLeft: '0.6rem' }}>
                    {text}
                </Typography>
            </Box>
        );
    };

    return (
        <div className={classes.root}>
            <Typography
                component="h3"
                variant="h3"
                align="center"
                className={classes.title}
            >
                Explore
            </Typography>
            <Grid container justifyContent="center" alignItems="center">
                <Link to="/login" className={classes.link}>
                    <Grid
                        item
                        xs={6}
                        className={clsx(
                            classes.box__student,
                            classes.box__common__add_border,
                        )}
                    >
                        <Typography align="center">Join as a</Typography>
                        <Typography
                            align="center"
                            component="h5"
                            variant="h5"
                            className={classes.box__typography}
                        >
                            student
                        </Typography>
                        <Box mt={4}>
                            {renderItem({ text: 'Easy account registration' })}
                            {renderItem({ text: 'Find a tutor quickly' })}
                            {renderItem({
                                text: 'Feel free to talk to your tutor at any time',
                            })}
                            {renderItem({
                                text: 'Chat with tutors through convenient messaging, video calling and file sending.',
                            })}
                        </Box>
                    </Grid>
                </Link>

                <Link to="/login" className={classes.link}>
                    <Grid
                        item
                        xs={6}
                        className={clsx(
                            classes.box__tutor,
                            classes.box__common__add_border,
                        )}
                    >
                        <Typography align="center">Join as a</Typography>
                        <Typography
                            align="center"
                            component="h5"
                            variant="h5"
                            className={classes.box__typography}
                        >
                            tutor
                        </Typography>
                        <Box mt={4}>
                            {renderItem({
                                text: 'Easy account registration',
                                variant: 'white',
                            })}
                            {renderItem({
                                text: 'Constantly receiving questions from students',
                                variant: 'white',
                            })}
                            {renderItem({
                                text: 'Feel free to talk to your student at any time',
                                variant: 'white',
                            })}
                            {renderItem({
                                text: 'Chat with student through convenient messaging, video calling and file sending.',
                                variant: 'white',
                            })}
                        </Box>
                    </Grid>
                </Link>
            </Grid>
            <Typography
                style={{ color: variables.colorWhite, margin: '6rem 0 1rem 0' }}
                component="p"
                align="center"
            >
                Download the app to your mobile phone now and learn.
            </Typography>
            <Box textAlign="center">
                <IOSButton margin={1} />
                <AndroidButton />
            </Box>
        </div>
    );
}

Summary.propTypes = {};

export default Summary;
