import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
    Box,
    Grid,
    Hidden,
    makeStyles,
    useTheme,
    useMediaQuery,
} from '@material-ui/core';
import Introduce from './Introduce';
import Menu from './Menu';
import Summary from './Summary';
import backgroundSmartphone from 'assets/images/backgroundSmartphone.svg';
import Brand from 'Common/Brand';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import backgroundSmartphoneMobile from 'assets/images/smartphone-mobile.svg';
import { variables } from 'Material/Style/colors';
import ButtonGroup from 'components/ButtonGroup';
import Loading from 'components/Loading';
import PayFastButton from 'Common/PayFast';

const useStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: '5%',
    },

    container: {
        height: '100%',
        width: '100%',
        boxSizing: 'border-box',
    },

    img: {
        width: '100%',
        [theme.breakpoints.down('sm')]: {
            width: '98%',
        },
    },

    img_mobile: {
        display: 'block',
        margin: '0 auto',
        width: '80%',
        // transform: 'scale(0.75, 0.5)',
    },

    hidden__lgUp__1: {
        [theme.breakpoints.up('lg')]: {
            justifyContent: 'space-between',
        },
    },

    link: {
        color: variables.bgMain,
        textDecoration: 'none',
        '&:hover': {
            backgroundColor: variables.bgMain,
        },
    },

    listItemIcon: {
        minWidth: 40,
    },
}));

function Home({ isAuthenticated, handleIsAuthenticated, user, loading }) {
    const classes = useStyles();
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down('sm'));

    return (
        <Grid container className={classes.container}>
            <Grid
                item
                xs={12}
                sm={12}
                md={5}
                lg={5}
                xl={5}
                className={classes.root}
            >
                <Box mt={3}>
                    <Brand />

                    <ButtonGroup
                        isAuthenticated={isAuthenticated}
                        handleIsAuthenticated={handleIsAuthenticated}
                        user={user}
                        loading={loading}
                    />
                </Box>
                <Hidden smDown>
                    {(loading && (
                        <Box position="relative" top="15%" left="15%">
                            <Loading />
                        </Box>
                    )) || (
                        <>
                            <Introduce />
                            <IOSButton margin={1} />
                            <AndroidButton />
                            <PayFastButton />
                        </>
                    )}
                </Hidden>
            </Grid>

            <Grid
                item
                xs={12}
                sm={12}
                md={7}
                lg={7}
                xl={7}
                style={{ textAlign: 'right', position: 'relative' }}
            >
                <Hidden mdUp>
                    <Box>
                        <img
                            src={backgroundSmartphoneMobile}
                            className={classes.img_mobile}
                            alt="background resolution mobile"
                        />
                    </Box>
                </Hidden>
                <Hidden smDown>
                    <img
                        src={backgroundSmartphone}
                        className={classes.img}
                        alt="hai sang"
                    />
                </Hidden>
            </Grid>
            <Hidden mdUp>
                <Grid
                    className="haisang"
                    md={12}
                    style={{ marginBottom: '2em' }}
                >
                    <Introduce />
                    <Hidden smDown>
                        <Box flexBasis="50%" textAlign="right">
                            <IOSButton margin="1.2" />
                            <AndroidButton margin="1.2" />
                        </Box>
                    </Hidden>
                    {matches && (
                        <Box width="100vw" textAlign="center" marginBottom={1}>
                            <IOSButton margin="1.2" />
                            <AndroidButton margin="1.2" />
                            <PayFastButton />
                        </Box>
                    )}
                </Grid>
            </Hidden>
            <Grid item xs={12}>
                <Menu />
                <Summary />
            </Grid>
        </Grid>
    );
}

Home.propTypes = {};

export default Home;
