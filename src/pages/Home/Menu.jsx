import React from 'react';
import PropTypes from 'prop-types';
import Card from 'Common/Card';
import { Box, Grid, makeStyles, Typography } from '@material-ui/core';
import Question from 'Common/Question';
import ELearning from 'Common/ELearning';
import Search from 'Common/Search';

const useStyles = makeStyles((theme) => ({
    root: {
        marginBottom: theme.spacing(15),
    },

    typography: {
        marginBottom: theme.spacing(2),
        fontWeight: 600,
    },
}));

function Menu(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Typography
                component="h3"
                align="center"
                variant="h3"
                className={classes.typography}
            >
                How PocketTutor works?
            </Typography>
            <Grid container justifyContent="center">
                <Card
                    component={<Question />}
                    question="1. Ask the tutor a question"
                    answer="Easily ask questions about any subject for tutors everywhere."
                />
                <Card
                    component={<Search />}
                    question="2. Quick tutor search system"
                    answer="Wait for the system to find the right tutor for you"
                />
                <Card
                    component={<ELearning />}
                    question="3. Learn"
                    answer="Communicate with tutors by asking questions, making voice calls, video calls and sending files at will."
                />
            </Grid>
        </div>
    );
}

Menu.propTypes = {};

export default Menu;
