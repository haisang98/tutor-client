import React from 'react';
import Tick from 'Common/Tick';
import MortarBoard from 'Common/MortarBoard';
import {
    Box,
    createTheme,
    makeStyles,
    ThemeProvider,
    responsiveFontSizes,
    Typography,
    useTheme,
    useMediaQuery,
} from '@material-ui/core';
import { variables } from 'Material/Style/colors';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';

const useStyles = makeStyles((theme) => ({
    button: {
        padding: theme.spacing(1, 4),
        borderRadius: theme.spacing(5),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },

    button__typography1: {
        textTransform: 'uppercase',
        fontSize: theme.spacing(1.3),
        opacity: 0.8,
    },

    button__typography2: {
        fontWeight: 500,
    },

    typography1: {
        fontWeight: 600,
        marginRight: theme.spacing(1),
        [theme.breakpoints.up('lg')]: {
            fontSize: theme.spacing(4.5),
        },
        [theme.breakpoints.down('lg')]: {
            fontSize: theme.spacing(3.6),
        },
        // [theme.breakpoints.down('sm')]: {
        //     fontSize: theme.spacing(3),
        // },
    },

    typography2: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3.5),
        color: variables.colorMain,

        [theme.breakpoints.up('lg')]: {
            fontSize: theme.spacing(4.5),
        },

        [theme.breakpoints.down('lg')]: {
            fontSize: theme.spacing(3.4),
        },
        // [theme.breakpoints.down('sm')]: {
        //     fontSize: theme.spacing(3),
        // },
    },

    typography__item: {
        // [theme.breakpoints.down('sm')]: {
        //     fontSize: theme.spacing(1.6),
        // },
    },

    mr_2: {
        marginRight: theme.spacing(2),
    },

    mt_1: {
        marginTop: theme.spacing(1),
    },
}));

function Introduce(props) {
    const classes = useStyles();
    const theme = useTheme();
    const matchesMobile = useMediaQuery('(max-width: 960px)');
    // const matchesRenderButton = useMediaQuery('(max-width: 845px)');

    const renderItem = ({ text = '', display = 'flex' }) => {
        return (
            <Box display={display} alignItems="start" mb={3}>
                <Tick variant="main" width={21} height={22} />
                <Typography
                    component="span"
                    className={classes.typography__item}
                    style={{ paddingLeft: '0.6em' }}
                >
                    {text}
                </Typography>
            </Box>
        );
    };

    if (matchesMobile)
        return (
            <Box
                display="flex"
                justifyContent="center"
                alignItems="center"
                flexDirection="column"
            >
                <Box display="flex" alignItems="center" justifyContent="center">
                    <Typography
                        component="h3"
                        variant="h3"
                        className={classes.typography1}
                    >
                        Online tutoring
                    </Typography>
                    <MortarBoard />
                </Box>
                <Typography
                    component="h3"
                    variant="h3"
                    className={classes.typography2}
                >
                    Any where. Any Time !
                </Typography>
                {renderItem({ text: 'On demand tutoring' })}
                {renderItem({ text: 'Learn from the best tutors' })}
                {renderItem({ text: 'Get help in any subject' })}
                {renderItem({ text: 'Sign up for free' })}
            </Box>
        );

    return (
        <Box marginTop={4.5}>
            <Box display="flex" alignItems="center">
                <Typography
                    component="h3"
                    variant="h3"
                    className={classes.typography1}
                >
                    Online tutoring
                </Typography>
                <MortarBoard />
            </Box>
            <Typography
                component="h3"
                variant="h3"
                className={classes.typography2}
            >
                Any where. Any Time !
            </Typography>
            {renderItem({ text: 'On demand tutoring' })}
            {renderItem({ text: 'Learn from the best tutors' })}
            {renderItem({ text: 'Get help in any subject' })}
            {renderItem({ text: 'Sign up for free' })}
        </Box>
    );
}

Introduce.propTypes = {};

export default Introduce;
