import axios from 'axios';
import { getLtik } from 'helper/LTI';
import queryString from 'query-string';
import { BASE_URL } from 'vars';

const axiosClient = axios.create({
    baseURL: BASE_URL,
    // timeout: 5000,
    params: { ltik: getLtik() },
    paramsSerializer: (params) =>
        queryString.stringify(params, {
            arrayFormat: 'bracket',
            skipNull: true,
            skipEmptyString: true,
        }),
});

axiosClient.interceptors.response.use(
    (response) => {
        if (response && response.data) {
            return response.data;
        }

        return response;
    },
    (error) => {
        if (error.code === 'ECONNABORTED') return Promise.reject(error);
        const { data } = error.response;
        return Promise.reject(data);
    },
);

// TODO: The Present, this set up request not apply.

// axiosClient.interceptors.request.use(async (config) => {
//     if (!config.headers.Authorization) {
//       const token = localStorage.getItem("token");
//     }

//     if (token) {
//       config.headers.Authorization = `Bearer ${token}`;
//     }

//     return config;
// });

export default axiosClient;
