import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import { variables } from 'Material/Style/colors';

const useStyles = makeStyles((theme) => ({
    '@keyframes loading': {
        '0%': {
            transform: 'rotate(-360deg)',
        },
        '100%': {
            transform: 'rotate(0deg)',
        },
    },

    root: {
        width: ({ size }) => size,
        height: ({ size }) => size,
        border: ({ variant }) => `${variant}px solid`,
        borderRadius: '100%',
        borderColor: ({ color }) => `${color} transparent ${color} transparent`,
        animation: '$loading 2s linear infinite',
    },
}));

function Loading({ size = 70, color = variables.bgMain, variant = 6 }) {
    const classes = useStyles({ size, color, variant });

    return <div className={classes.root} />;
}

Loading.propTypes = {};

export default Loading;
