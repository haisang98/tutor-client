import React from 'react';
import PropTypes from 'prop-types';
import {
    FormControl,
    FormHelperText,
    Input,
    InputLabel,
    makeStyles,
} from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1.5, 0),
    },
}));

function InputField({
    htmlFor,
    idHelperText,
    nameHelperText,
    label,
    name,
    type = 'text',
    onChange,
}) {
    const classes = useStyles();

    return (
        <FormControl fullWidth className={classes.formControl}>
            <InputLabel htmlFor={htmlFor}>{label}</InputLabel>
            <Input
                type={type}
                id={htmlFor}
                aria-describedby={idHelperText}
                name={name}
                onChange={onChange}
            />
            <FormHelperText id={idHelperText}>{nameHelperText}</FormHelperText>
        </FormControl>
    );
}

InputField.propTypes = {};

export default InputField;
