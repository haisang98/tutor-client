import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
    FormControl,
    FormHelperText,
    InputLabel,
    Input,
    makeStyles,
    Typography,
    Button,
} from '@material-ui/core';

/* ------------------------------------------- */
import ButtonLogin from 'Material/Button/Login';
import ButtonRegister from 'Material/Button/Register';
import ButtonForgot from 'Material/Button/Forgot';
import { STATUS } from 'vars';
import InputField from 'components/Test/InputField';
import useForm from 'hook/useForm';
import validate from 'validation/login';
import API from 'apis';
import { getToken, setToken } from 'helpers/token';

const useStyles = makeStyles((theme) => ({
    root: {
        transform: 'translate(0px, -4rem)',
    },

    title: {
        fontWeight: 600,
        fontSize: theme.spacing(4.5),
    },

    groupButton: {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: theme.spacing(5),
    },

    button: {
        textTransform: 'capitalize',
    },

    button__login: {
        flexBasis: '100%',
        marginBottom: theme.spacing(2),
    },

    button__register: {
        flexBasis: '49%',
        marginRight: theme.spacing(0.5),
    },

    button__forgot: {
        flexBasis: '49%',
        marginLeft: theme.spacing(0.5),
    },
}));

const initialState = {
    email: '',
    password: '',
};

function Login({ handleChangeStatusLogin, authenticated }) {
    const classes = useStyles();

    const handleLogin = async () => {
        try {
            const dataLogin = await API.login(values);
            const { accessToken } = dataLogin.data;

            setToken(accessToken);

            authenticated(true);
        } catch (error) {
            console.error(error);
        }
    };

    const { errors, handleChange, handleSubmit, values } = useForm(
        handleLogin,
        validate,
        initialState,
    );

    return (
        <div className={classes.root}>
            <form onSubmit={handleSubmit}>
                <Typography
                    component="h3"
                    variant="subtitle1"
                    className={classes.title}
                >
                    Login
                </Typography>

                <InputField
                    htmlFor="email-address"
                    idHelperText="email-address-helper-text"
                    nameHelperText="We&#39;ll never share your email."
                    label="Email address"
                    name="email"
                    onChange={handleChange}
                />

                <InputField
                    htmlFor="password"
                    idHelperText="password-helper"
                    nameHelperText="We&#39;ll never share your email."
                    label="Password"
                    name="password"
                    type="password"
                    onChange={handleChange}
                />

                <div className={classes.groupButton}>
                    <ButtonLogin
                        type="submit"
                        className={clsx(classes.button, classes.button__login)}
                    >
                        Login
                    </ButtonLogin>
                    <ButtonRegister
                        onClick={() => handleChangeStatusLogin(STATUS.REGISTER)}
                        className={clsx(
                            classes.button,
                            classes.button__register,
                        )}
                    >
                        Register Now !
                    </ButtonRegister>
                    <ButtonForgot
                        className={clsx(classes.button, classes.button__forgot)}
                    >
                        Forgot Password
                    </ButtonForgot>
                </div>
            </form>
        </div>
    );
}

Login.propTypes = {};

export default Login;
