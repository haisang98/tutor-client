import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
    FormControl,
    FormHelperText,
    InputLabel,
    Input,
    makeStyles,
    Typography,
    Button,
} from '@material-ui/core';

/* ------------------------------------------- */
import ButtonLogin from 'Material/Button/Login';
import ButtonRegister from 'Material/Button/Register';
import ButtonForgot from 'Material/Button/Forgot';
import { STATUS } from 'vars';

const useStyles = makeStyles((theme) => ({
    root: {
        transform: 'translate(0px, -4rem)',
    },

    title: {
        fontWeight: 600,
        fontSize: theme.spacing(4.5),
    },

    formControl: {
        margin: theme.spacing(1.5, 0),
    },

    groupButton: {
        display: 'flex',
        flexWrap: 'wrap',
        marginTop: theme.spacing(5),
    },

    button: {
        textTransform: 'capitalize',
    },

    button__register: {
        flexBasis: '100%',
        marginBottom: theme.spacing(2),
    },

    button__login: {
        flexBasis: '49%',
        marginRight: theme.spacing(0.5),
    },

    button__forgot: {
        flexBasis: '49%',
        marginLeft: theme.spacing(0.5),
    },
}));

function Register({ handleChangeStatusLogin }) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <form>
                <Typography
                    component="h3"
                    variant="subtitle1"
                    className={classes.title}
                >
                    Register
                </Typography>
                <FormControl fullWidth className={classes.formControl}>
                    <InputLabel htmlFor="email-address">
                        Email address
                    </InputLabel>
                    <Input
                        id="email-address"
                        aria-describedby="email-address-helper-text"
                    />
                    <FormHelperText id="email-address-helper-text">
                        We&#39;ll never share your email.
                    </FormHelperText>
                </FormControl>
                <FormControl fullWidth className={classes.formControl}>
                    <InputLabel htmlFor="password">Password</InputLabel>
                    <Input id="password" aria-describedby="password-helper" />
                    <FormHelperText id="password-helper">
                        We&#39;ll never share your password.
                    </FormHelperText>
                </FormControl>

                <div className={classes.groupButton}>
                    <ButtonLogin
                        className={clsx(
                            classes.button,
                            classes.button__register,
                        )}
                    >
                        Register
                    </ButtonLogin>
                    <ButtonRegister
                        onClick={() => handleChangeStatusLogin(STATUS.LOGIN)}
                        className={clsx(classes.button, classes.button__login)}
                    >
                        Login Now !
                    </ButtonRegister>
                    <ButtonForgot
                        className={clsx(classes.button, classes.button__forgot)}
                    >
                        Forgot Password
                    </ButtonForgot>
                </div>
            </form>
        </div>
    );
}

Register.propTypes = {};

export default Register;
