import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

import Title from './Title';
import OptionsGroup from './OptionsGroup';
import Content from './Content';

const useStyles = makeStyles((theme) => ({
    container: {
        maxWidth: '70rem',
        width: '100%',
        maxHeight: '85vh',
        height: '100%',
        borderRadius: theme.spacing(2),
        border: '3px solid #696F79',
        backgroundColor: '#FFFFFF',
    },
}));

function Conversation(props) {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <Title />
            <OptionsGroup />
            <Content />
        </div>
    );
}

Conversation.propTypes = {};

export default Conversation;
