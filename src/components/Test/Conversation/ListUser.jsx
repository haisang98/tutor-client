import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, Typography } from '@material-ui/core';
import { blueGrey } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    title__list_user_paticipated: {
        borderBottom: '1px solid',
        padding: theme.spacing(1, 0),
    },

    list: {
        borderRight: '1px solid',
        flexBasis: '25%',
    },

    list__user: {
        backgroundColor: blueGrey[700],
        color: blueGrey[50],
    },

    list__item: {
        padding: theme.spacing(1),
    },
}));

function ListUser(props) {
    const classes = useStyles();

    return (
        <div className={classes.list}>
            <Typography
                className={classes.title__list_user_paticipated}
                component="h4"
                align="center"
            >
                List User Paticipated
            </Typography>
            <div className={classes.list__user}>
                <div className={classes.list__item}>Hai Sang</div>
                <div className={classes.list__item}>Van Thanh</div>
            </div>
        </div>
    );
}

ListUser.propTypes = {};

export default ListUser;
