import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, Button } from '@material-ui/core';
import { lightBlue, blueGrey } from '@material-ui/core/colors';
import AddIcon from '@material-ui/icons/Add';

/* ------------------------------ */
import API from 'apis';
import { setTokenFromTwillo } from 'helpers/token';

const useStyles = makeStyles((theme) => ({
    root: {
        marginLeft: theme.spacing(2),
        marginRight: theme.spacing(2),
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(5),
        display: 'flex',
        justifyContent: 'space-between',
    },

    button__create: {
        textTransform: 'capitalize',
        backgroundColor: lightBlue[300],

        '&:hover': {
            backgroundColor: lightBlue[400],
        },
    },

    participant: {
        display: 'flex',
    },

    participant__input: {
        border: `1px solid ${blueGrey[700]}`,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,

        '& > input': {
            border: 'none',
            minHeight: '100%',
            borderTopLeftRadius: 20,
            borderBottomLeftRadius: 20,
            paddingLeft: theme.spacing(1),
            outline: 'none',
        },
    },

    button__join: {
        textTransform: 'capitalize',
        backgroundColor: blueGrey[500],
        color: blueGrey[50],
        fontWeight: 600,
        borderTopLeftRadius: 0,
        borderBottomLeftRadius: 0,
        borderTopRightRadius: 20,
        borderBottomRightRadius: 20,

        '&:hover': {
            backgroundColor: blueGrey[700],
        },
    },
}));

function OptionsGroup(props) {
    const classes = useStyles();

    const handleCreateConversation = async () => {
        try {
            const result = await API.createConversation();

            if (result && result.sid) {
                const fetchConversation = await API.fetchConversation(
                    result.sid,
                );

                if (fetchConversation && fetchConversation.chatServiceSid) {
                    const requestToken = await API.requestToken(
                        fetchConversation.chatServiceSid,
                    );

                    if (requestToken.status === 200) {
                        setTokenFromTwillo(requestToken.data.token);
                    }
                }
            }
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className={classes.root}>
            <Button
                startIcon={<AddIcon />}
                className={classes.button__create}
                onClick={handleCreateConversation}
            >
                Create Conversation
            </Button>

            <div className={classes.participant}>
                <div className={classes.participant__input}>
                    <input
                        type="text"
                        name="participant"
                        placeholder="Enter participant ID"
                    />
                </div>
                <Button className={classes.button__join}>Join</Button>
            </div>
        </div>
    );
}

OptionsGroup.propTypes = {};

export default OptionsGroup;
