import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, Typography } from '@material-ui/core';
import { blueGrey, indigo, lightBlue, grey } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    body: {
        position: 'relative',
        flexBasis: '75%',
    },

    body__input: {
        position: 'absolute',
        bottom: 0,
        height: theme.spacing(5),
        width: '100%',
        border: `1px solid ${lightBlue[400]}`,
        borderBottom: 'none',
        borderLeft: 'none',
        borderRight: 'none',

        '& > input': {
            border: 'none',
            outline: 'none',
            height: '100%',
            paddingLeft: theme.spacing(1),
            fontSize: theme.spacing(1.8),
        },
    },

    body__title: {
        borderTopRightRadius: 18,
        paddingRight: theme.spacing(2.5),
        paddingTop: theme.spacing(1),
        paddingBottom: theme.spacing(1),
        backgroundColor: indigo[500],
        color: indigo[50],
        fontWeight: 600,
        fontSize: theme.spacing(2),
    },

    body__text: {
        padding: theme.spacing(2, 0),
    },

    body__textItem__me: {
        textAlign: 'end',
    },

    body__textItem__you: {
        textAlign: 'start',
    },

    body__textItem__typography__me: {
        marginRight: theme.spacing(1),
        backgroundColor: indigo[700],
        color: indigo[50],
        padding: theme.spacing(1),
        borderRadius: 20,
    },

    body__textItem__typography__you: {
        marginLeft: theme.spacing(1),
        backgroundColor: blueGrey[200],
        color: grey[900],
        padding: theme.spacing(1),
        borderRadius: 20,
    },
}));

function Body(props) {
    const classes = useStyles();

    return (
        <div className={classes.body}>
            <Typography
                component="h4"
                align="right"
                className={classes.body__title}
            >
                List User Paticipated
            </Typography>
            <div className={classes.body__text}>
                <div className={classes.body__textItem__me}>
                    <Typography
                        component="span"
                        className={classes.body__textItem__typography__me}
                    >
                        Hello, My name is sang
                    </Typography>
                </div>

                <div className={classes.body__textItem__you}>
                    <Typography
                        component="span"
                        className={classes.body__textItem__typography__you}
                    >
                        Hello, My name is thanh
                    </Typography>
                </div>
            </div>
            <div className={classes.body__input}>
                <input
                    type="text"
                    name="text"
                    placeholder="Typing something with others..."
                />
            </div>
        </div>
    );
}

Body.propTypes = {};

export default Body;
