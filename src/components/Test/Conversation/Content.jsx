import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

/* ------------------------------------------ */
import ListUser from './ListUser';
import Body from './Body';

const useStyles = makeStyles((theme) => ({
    root: {
        boxSizing: 'border-box',
        display: 'flex',
        minHeight: '80%',
        borderRadius: 20,
        margin: theme.spacing(0, 2),
        border: '1px solid black',
    },
}));

function Content(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <ListUser />
            <Body />
        </div>
    );
}

Content.propTypes = {};

export default Content;
