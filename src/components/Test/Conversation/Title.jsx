import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';
import { deepPurple } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    root: {
        fontSize: theme.spacing(5.5),
        fontWeight: 600,
        textTransform: 'capatilize',
        textAlign: 'center',
        color: deepPurple[50],
        backgroundColor: deepPurple[400],
        borderTopLeftRadius: theme.spacing(1.3),
        borderTopRightRadius: theme.spacing(1.3),
    },
}));

function Title(props) {
    const classes = useStyles();

    return <div className={classes.root}>Conversation</div>;
}

Title.propTypes = {};

export default Title;
