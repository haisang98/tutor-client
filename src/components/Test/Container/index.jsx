import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

/* ------------------------------------------- */
import BackgroundCity from 'assets/images/background.jpg';
import Login from 'components/Test/Login';
import Register from 'components/Test/Register';
import { STATUS } from 'vars';
import Conversation from 'components/Test/Conversation';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
    },

    container: {
        display: 'flex',
        maxWidth: '70rem',
        width: '100%',
        maxHeight: '85vh',
        height: '100%',
        borderRadius: theme.spacing(2),
        border: '3px solid #696F79',
        backgroundColor: '#FFFFFF',
    },

    left: {
        flexBasis: '40%',
        backgroundColor: '#62959C',
        backgroundImage: `url(${BackgroundCity})`,
        backgroundBlendMode: 'overlay',
        backgroundSize: 'cover',
        borderTopLeftRadius: theme.spacing(1.6),
        borderBottomLeftRadius: theme.spacing(1.6),
    },

    right: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexBasis: '60%',
        borderTopLeftRadius: theme.spacing(2),
        borderBottomLeftRadius: theme.spacing(2),
    },
}));

function Container(props) {
    const classes = useStyles();

    const [status, setStatus] = useState(STATUS.LOGIN);
    const [isAuthenticated, setIsAuthenticated] = useState(false);

    const handleChangeStatusLogin = (status) => {
        setStatus(status);
    };

    const handleChangeStatusAuthenticated = (isAuth) => {
        setIsAuthenticated(isAuth);
    };

    return (
        <div className={classes.root}>
            {isAuthenticated && <Conversation />}
            {!isAuthenticated && (
                <div className={classes.container}>
                    <div className={classes.left} />
                    <div className={classes.right}>
                        {status === STATUS.LOGIN && (
                            <Login
                                handleChangeStatusLogin={
                                    handleChangeStatusLogin
                                }
                                authenticated={handleChangeStatusAuthenticated}
                            />
                        )}
                        {status === STATUS.REGISTER && (
                            <Register
                                handleChangeStatusLogin={
                                    handleChangeStatusLogin
                                }
                                authenticated={handleChangeStatusAuthenticated}
                            />
                        )}
                    </div>
                </div>
            )}
        </div>
    );
}

Container.propTypes = {};

export default Container;
