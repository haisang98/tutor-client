import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

/* ------------------------------------------ */
import Container from 'components/Test/Container';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
        backgroundColor: '#000000',
    },
}));

function Home(props) {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Container />
        </div>
    );
}

Home.propTypes = {};

export default Home;
