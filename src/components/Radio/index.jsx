import React from 'react';
import PropTypes from 'prop-types';
import { styled, makeStyles } from '@material-ui/styles';
import RadioGroup from '@material-ui/core/RadioGroup';
import MuiRadio from '@material-ui/core/Radio';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import { variables } from 'Material/Style/colors';

const StyledFormControlLabel = styled((props) => (
    <FormControlLabel {...props} />
))(({ theme, checked }) => ({
    '.MuiFormControlLabel-label': checked && {
        color: variables.bgMain,
    },

    '.MuiButtonBase-root': {
        color: variables.bgMain,
    },
}));

const useStyles = makeStyles((theme) => ({
    radio: {
        '& .MuiRadio-colorPrimary.Mui-checked': {
            color: variables.bgMain,
        },
    },
}));

function Radio({ value = 'tutor', onChange }) {
    const classes = useStyles();

    return (
        <RadioGroup
            row
            style={{ margin: '1rem 0' }}
            value={value}
            onChange={onChange}
        >
            <StyledFormControlLabel
                value="tutor"
                label="Tutor"
                className={classes.radio}
                control={<MuiRadio color="primary" />}
            />
            <StyledFormControlLabel
                value="student"
                label="Student"
                className={classes.radio}
                control={<MuiRadio color="primary" />}
            />
        </RadioGroup>
    );
}

Radio.propTypes = {};

export default Radio;
