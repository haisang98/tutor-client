import React from 'react';
import PropTypes from 'prop-types';
import Button from 'Material/Button/Base';
import clsx from 'clsx';
import { variables } from 'Material/Style/colors';
import { Box, makeStyles, Typography } from '@material-ui/core';
import Apple from 'Common/Apple';

const useStyles = makeStyles((theme) => ({
    button: {
        padding: theme.spacing(1, 4),
        borderRadius: theme.spacing(5),

        '&:hover': {
            color: variables.colorWhite,
            backgroundColor: variables.bgMainDark,
        },
    },

    button__typography1: {
        textTransform: 'uppercase',
        fontSize: theme.spacing(1.3),
        opacity: 0.8,
    },

    button__typography2: {
        fontWeight: 500,
    },

    mr_2: {
        marginRight: ({ margin }) => theme.spacing(margin),
    },

    mt_1: {
        marginTop: theme.spacing(1),
    },
}));

function IOSButton({ margin = 0 }) {
    const classes = useStyles({ margin });

    return (
        <Button className={clsx(classes.button, classes.mr_2, classes.mt_1)}>
            <Apple />
            <Box ml={1}>
                <Typography
                    align="left"
                    className={classes.button__typography1}
                >
                    get from
                </Typography>
                <Typography
                    align="left"
                    className={classes.button__typography2}
                >
                    Appstore
                </Typography>
            </Box>
        </Button>
    );
}

IOSButton.propTypes = {};

export default IOSButton;
