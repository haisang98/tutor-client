import React from 'react';
import PropTypes from 'prop-types';
import { Box, InputLabel, makeStyles } from '@material-ui/core';
import { variables } from 'Material/Style/colors';

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(0),
        marginBottom: ({ mb }) => theme.spacing(mb) || 0,
    },

    input: {
        minHeight: theme.spacing(4),
        border: `1px solid ${variables.borderColorDefault}`,
        borderRadius: variables.brRadiusStandard,
        // minWidth: theme.spacing(35),
        width: '100%',

        '& > input': {
            border: 'none',
            outline: 'none',
            borderRadius: variables.brRadiusStandard,
            width: '100%',
            height: theme.spacing(4.2),
            paddingLeft: theme.spacing(1),
        },

        '&:focus-within': {
            borderColor: variables.bgMain,
        },
    },

    label: {
        transform: 'scale(0.75)',
        paddingLeft: theme.spacing(0.4),
        marginBottom: theme.spacing(0.5),
    },
}));

function InputField({
    htmlFor,
    idHelperText,
    mb,
    label,
    name,
    type = 'text',
    value,
    onChange,
}) {
    const classes = useStyles({ mb });

    return (
        <Box className={classes.formControl}>
            <InputLabel htmlFor={htmlFor} className={classes.label}>
                {label}
            </InputLabel>
            <div className={classes.input}>
                <input
                    type={type}
                    id={htmlFor}
                    aria-describedby={idHelperText}
                    name={name}
                    value={value}
                    onChange={onChange}
                />
            </div>
        </Box>
    );
}

InputField.propTypes = {};

export default InputField;
