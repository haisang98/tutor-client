import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
    Box,
    Grid,
    Hidden,
    makeStyles,
    useTheme,
    useMediaQuery,
    withStyles,
    Typography,
} from '@material-ui/core';

import backgroundSmartphone from 'assets/images/backgroundSmartphone.svg';
import Brand from 'Common/Brand';
import SignIn from 'Material/Button/Login';
import Register from 'Material/Button/Register';
import clsx from 'clsx';
import { Link } from 'react-router-dom';
import IOSButton from 'components/Button/IOS';
import AndroidButton from 'components/Button/Android';
import backgroundSmartphoneMobile from 'assets/images/smartphone-mobile.svg';
import MobileMenu from 'pages/Home/MobileMenu';
import Button from 'Material/Button/Base';
import MenuBased from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import { FcMenu } from 'react-icons/fc';
import { FaUserPlus } from 'react-icons/fa';
import { RiLoginCircleFill, RiLogoutBoxFill } from 'react-icons/ri';
import { variables } from 'Material/Style/colors';
import { removeToken } from 'helpers/token';
import { useHistory } from 'react-router-dom';
import { teal } from '@material-ui/core/colors';
import Loading from 'components/Loading';

const StyledMenu = withStyles({
    paper: {
        border: '1px solid #d3d4d5',
    },
})((props) => (
    <MenuBased
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
        }}
        transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
        }}
        {...props}
    />
));

const StyledMenuItem = withStyles((theme) => ({
    root: {
        '&:focus': {
            backgroundColor: variables.bgMain,
            '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                color: theme.palette.common.white,
            },
        },
    },
}))(MenuItem);

const useStyles = makeStyles((theme) => ({
    root: {
        paddingLeft: '5%',
    },

    container: {
        height: '100%',
        width: '100%',
        boxSizing: 'border-box',
    },

    img: {
        width: '100%',
        [theme.breakpoints.down('sm')]: {
            width: '98%',
        },
    },

    img_mobile: {
        display: 'block',
        margin: '0 auto',
        width: '80%',
        // transform: 'scale(0.75, 0.5)',
    },

    hidden__lgUp__1: {
        [theme.breakpoints.up('lg')]: {
            justifyContent: 'space-between',
        },
    },

    link: {
        color: variables.bgMain,
        textDecoration: 'none',
        '&:hover': {
            backgroundColor: variables.bgMain,
        },
    },

    listItemIcon: {
        minWidth: 40,
    },

    button: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(2),
    },

    logout: {
        backgroundColor: '#FFFFFF',
        color: variables.bgMain,
        marginLeft: theme.spacing(1),
        paddingLeft: theme.spacing(1.5),
        paddingRight: theme.spacing(1.5),
        border: `1px solid white`,

        '&:hover': {
            backgroundColor: 'unset',
            color: `${variables.colorWhite}`,
        },

        [theme.breakpoints.up('sm')]: {
            color: '#FFFFFF',
            backgroundColor: variables.bgMain,

            '&:hover': {
                backgroundColor: variables.bgMainDark,
            },
        },
    },
}));

export default function ButtonCustom({
    isAuthenticated,
    handleIsAuthenticated,
    user,
    loading,
}) {
    const classes = useStyles();
    const matchesRenderButton = useMediaQuery('(max-width: 845px)');
    const matchesBelowMd = useMediaQuery('(max-width: 959px)');
    const [anchorEl, setAnchorEl] = useState(null);
    const history = useHistory();

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    const handleOnLogout = () => {
        removeToken();
        handleIsAuthenticated(false);
        history.push('/');
    };

    return (
        <Box
            position="absolute"
            zIndex={100}
            top={0}
            right={0}
            paddingRight={(matchesRenderButton && 2) || 7}
            paddingTop={3}
        >
            <Hidden xsDown>
                {(isAuthenticated && (
                    <Box display="flex" alignItems="center">
                        <Typography
                            style={
                                (matchesBelowMd && {
                                    fontSize: '0.8em',
                                    color: '#000000',
                                }) || { fontSize: '0.8em', color: '#FFFFFF' }
                            }
                        >
                            {(loading && (
                                <Loading
                                    size={15}
                                    color={
                                        (matchesBelowMd && '#000000') ||
                                        '#FFFFFF'
                                    }
                                    variant={2}
                                />
                            )) || (
                                <>
                                    <span style={{ opacity: '0.9' }}>Hi,</span>{' '}
                                    <b>{user && user.split('@')[0]}</b>
                                </>
                            )}
                        </Typography>
                        <Button
                            className={clsx(classes.logout)}
                            onClick={handleOnLogout}
                        >
                            Logout
                        </Button>
                    </Box>
                )) || (
                    <>
                        <Link
                            to="/login"
                            style={{
                                textDecoration: 'none',
                                marginRight: '0.3em',
                            }}
                        >
                            <SignIn
                                className={clsx(classes.signIn, classes.button)}
                            >
                                sign in
                            </SignIn>
                        </Link>
                        <Link to="/register" style={{ textDecoration: 'none' }}>
                            <Register
                                className={clsx(classes.signIn, classes.button)}
                            >
                                register
                            </Register>
                        </Link>
                    </>
                )}
            </Hidden>

            <Hidden smUp>
                <FcMenu style={{ fontSize: '1.5rem' }} onClick={handleClick} />

                <StyledMenu
                    id="customized-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                >
                    {(!isAuthenticated && (
                        <>
                            <Link
                                to="/login"
                                className={classes.link}
                                style={{
                                    marginRight: '0.3em',
                                }}
                            >
                                <StyledMenuItem>
                                    <ListItemIcon
                                        className={classes.listItemIcon}
                                    >
                                        <RiLoginCircleFill
                                            fontSize="1.25rem"
                                            color={variables.bgMain}
                                        />
                                    </ListItemIcon>
                                    <ListItemText primary="Login" />
                                </StyledMenuItem>
                            </Link>
                            <Link to="/register" className={classes.link}>
                                <StyledMenuItem>
                                    <ListItemIcon
                                        className={classes.listItemIcon}
                                    >
                                        <FaUserPlus
                                            fontSize="1.25rem"
                                            color={variables.bgMain}
                                        />
                                    </ListItemIcon>
                                    <ListItemText primary="Register" />
                                </StyledMenuItem>
                            </Link>
                        </>
                    )) || (
                        <>
                            <Typography
                                style={{
                                    fontSize: '0.85em',
                                    color: '#000',
                                    padding: '0.3rem 0.75rem',
                                }}
                            >
                                <b>{user && user.split('@')[0]}</b>
                            </Typography>
                            <StyledMenuItem onClick={handleOnLogout}>
                                <ListItemIcon className={classes.listItemIcon}>
                                    <RiLogoutBoxFill
                                        fontSize="1.25rem"
                                        color={teal[900]}
                                    />
                                </ListItemIcon>
                                <ListItemText
                                    primary="Logout"
                                    style={{ color: teal[900] }}
                                />
                            </StyledMenuItem>
                        </>
                    )}
                </StyledMenu>
            </Hidden>
        </Box>
    );
}
