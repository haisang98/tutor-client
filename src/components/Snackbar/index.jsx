import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

export default function CustomizedSnackbars({
    snackbar,
    handleOnClose,
    isActive,
}) {
    const classes = useStyles();
    console.log({ isActive });
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        handleOnClose();
    };

    return (
        <div className={classes.root}>
            {isActive && (
                <Snackbar
                    open={snackbar.open}
                    autoHideDuration={2000}
                    onClose={handleClose}
                    anchorOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <Alert
                        onClose={handleClose}
                        severity={
                            (snackbar.statusCode >= 200 &&
                                snackbar.statusCode < 400 &&
                                'success') ||
                            (snackbar.statusCode >= 400 && 'error') ||
                            (!snackbar.statusCode && 'error') ||
                            'info'
                        }
                    >
                        {snackbar.message}
                    </Alert>
                </Snackbar>
            )}
        </div>
    );
}
