import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import { variables } from 'Material/Style/colors';

const useStyles = makeStyles((theme) => ({
    formControl: {
        marginBottom: theme.spacing(3),
        minWidth: 120,
    },

    selectField: {
        marginTop: theme.spacing(2.5),
        height: theme.spacing(4.5),
        borderRadius: variables.brRadiusStandard,

        '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
            borderColor: variables.bgMain,
            borderWidth: '1px',
        },

        '& .MuiSelect-select:focus': {
            backgroundColor: 'unset',
        },
    },

    dropdownStyle: {},
}));

function SelectField({ htmlFor, onChange, label, name, value }) {
    const classes = useStyles();

    return (
        <FormControl fullWidth className={classes.formControl}>
            <InputLabel shrink id={htmlFor}>
                {label}
            </InputLabel>
            <Select
                variant="outlined"
                labelId={htmlFor}
                id={htmlFor}
                MenuProps={{
                    disableScrollLock: true,
                    getContentAnchorEl: null,
                    PaperProps: { style: { transform: 'translateY(30%)' } },
                    anchorOrigin: { vertical: 'top', horizontal: 'center' },
                    transformOrigin: {
                        vertical: 'top',
                        horizontal: 'center',
                    },
                }}
                value={value}
                name={name}
                onChange={onChange}
                displayEmpty
                className={classes.selectField}
            >
                <MenuItem value="">
                    <em>--Select Role--</em>
                </MenuItem>
                <MenuItem value="tutor">Tutor</MenuItem>
                <MenuItem value="student">Student</MenuItem>
            </Select>
        </FormControl>
    );
}

SelectField.propTypes = {};

export default SelectField;
