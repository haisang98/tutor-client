import React from 'react';
import PropTypes from 'prop-types';
import Brand from 'Common/Brand';
import SignIn from 'Material/Button/Login';
import Register from 'Material/Button/Register';
import { Box, Grid, makeStyles } from '@material-ui/core';
import clsx from 'clsx';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
    root: {
        // marginBottom: theme.spacing(6),
    },

    button: {
        padding: theme.spacing(0.65, 1.5),
    },

    signIn: {
        marginRight: theme.spacing(1),
    },
}));

function Header(props) {
    const classes = useStyles();

    return (
        <Grid container justifyContent="space-between" className={classes.root}>
            <Grid item>
                <Brand />
            </Grid>
            <Grid item>
                <Link to="/login" style={{ textDecoration: 'none' }}>
                    <SignIn className={clsx(classes.signIn, classes.button)}>
                        sign in
                    </SignIn>
                </Link>
                <Link to="/register" style={{ textDecoration: 'none' }}>
                    <Register className={clsx(classes.signIn, classes.button)}>
                        register
                    </Register>
                </Link>
            </Grid>
        </Grid>
    );
}

Header.propTypes = {};

export default Header;
