import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
    },

    message: {
        fontWeight: 400,
        fontStyle: 'italic',
        fontSize: '2.5em',
    },

    status: {
        marginBottom: 8,
        display: 'inline-block',
        borderBottom: '2px solid',
        fontWeight: 600,
        fontSize: '3.5em',
    },
}));

function Error(props) {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <h3 className={classes.status}>404</h3>
            <h3 className={classes.message}>Not Found</h3>
        </div>
    );
}

Error.propTypes = {};

export default Error;
