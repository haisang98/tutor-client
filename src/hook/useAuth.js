const { getToken } = require('helpers/token');
const { useState, useEffect } = require('react');

const useAuth = () => {
    const [tokenString, setTokenString] = useState('');

    useEffect(() => {
        setTokenString(getToken());
    }, [tokenString]);

    return (
        (tokenString && { isAuthenticated: true }) || { isAuthenticated: false }
    );
};

export default useAuth;
