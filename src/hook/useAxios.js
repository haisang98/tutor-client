import { useState, useEffect } from 'react';
import axiosClient from 'config/axiosClient';

const isEmptyObject = (query) =>
    (query &&
        Object.keys(query).length === 0 &&
        query.constructor === Object) ||
    false;

const useFetch = ({ method, url, params = {}, body = {} }) => {
    const [data, setData] = useState(null);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(false);
    const [reload, setReload] = useState(false);

    const _param = isEmptyObject(params) ? '{}' : JSON.stringify(params);
    const _body = isEmptyObject(body) ? '{}' : JSON.stringify(body);

    // const _param = params !== {} ? JSON.stringify(params) : {};
    // const _body = body !== {} ? JSON.stringify(body) : {};

    const callback = (bool) => {
        setReload(bool);
    };

    useEffect(() => {
        const fetchData = async () => {
            setLoading(true);
            setError(null);
            try {
                setLoading(true);
                const result = await axiosClient({
                    url,
                    params: JSON.parse(_param),
                    data: JSON.parse(_body),
                });
                setData(result);

                setLoading(false);
            } catch (err) {
                console.log(err);
                setLoading(false);
                setData(null);
                setError(err);
            }
        };
        fetchData();
    }, [method, url, _param, _body, reload]);

    return { data, error, loading, callback };
};

export default useFetch;
