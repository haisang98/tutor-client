import { useState, useEffect } from 'react';

const useForm = (callback, validate, initialState) => {
    const [errors, setErrors] = useState(initialState);
    const [values, setValues] = useState(initialState);
    const [isSubmitting, setIsSubmitting] = useState(false);

    const handleSubmit = (event) => {
        if (event) event.preventDefault();
        setErrors(validate(values));
        setIsSubmitting(true);
    };

    const handleChange = (event) => {
        event.persist();
        const { name, value } = event.target;

        setValues((values) => ({
            ...values,
            [name]: value,
        }));
    };

    useEffect(() => {
        if (Object.keys(errors).length === 0 && isSubmitting) {
            callback();
        }
    }, [errors]);

    return {
        handleChange,
        handleSubmit,
        values,
        errors,
    };
};

export default useForm;
