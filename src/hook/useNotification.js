import { useState } from 'react';

export const useNotification = () => {
    const [snackbar, setSnackbar] = useState({
        open: false,
        statusCode: null,
        message: '',
    });

    const handleOpenSnackbar = ({
        statusCode = 200,
        message = 'Successfully',
    }) => {
        setSnackbar({
            open: true,
            statusCode,
            message,
        });
    };

    const handleCloseSnackbar = () => {
        setSnackbar({
            open: false,
            statusCode: null,
            message: '',
        });
    };

    return {
        snackbar,
        handleOpenSnackbar,
        handleCloseSnackbar,
    };
};
