import { useState, useEffect } from 'react';

/**
 * Delay updating a value
 * @param {any} value - value to be delayed when updating
 * @param {number} delay - time to delay in ms
 * @return delayed value
 */
export default function useDebounce(value, delay) {
    const [debouncedValue, setDebouncedValue] = useState(value);

    useEffect(() => {
        const handler = setTimeout(() => {
            setDebouncedValue(value);
        }, delay);

        return () => {
            clearTimeout(handler);
        };
    }, [value, delay]);

    return debouncedValue;
}
